Rails.application.routes.draw do
  root to: "dashboard#index"


namespace :api, defaults: {format: :json} do
  namespace :v1 do
    resources :light_measurements
    resources :indoor_temperatures
    resources :outdoor_temperatures
  end
end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
