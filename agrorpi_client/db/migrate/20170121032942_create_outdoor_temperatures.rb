class CreateOutdoorTemperatures < ActiveRecord::Migration[5.0]
  def change
    create_table :outdoor_temperatures do |t|
      t.decimal :celsius
      t.decimal :farenheit
      t.datetime :measured_at

      t.timestamps
    end
  end
end
