class CreateLightMeasurements < ActiveRecord::Migration[5.0]
  def change
    create_table :light_measurements do |t|
      t.integer :lux
      t.datetime :measure_time

      t.timestamps
    end
  end
end
