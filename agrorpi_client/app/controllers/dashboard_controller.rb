class DashboardController < ApplicationController
  def index
  	@outdoor_graph = OutdoorTemperature.all
  	@indoor_graph = IndoorTemperature.all

	@outdoor = @outdoor_graph.order("measured_at DESC")
	@indoor = @indoor_graph.order("measured_at DESC")
  end
end
