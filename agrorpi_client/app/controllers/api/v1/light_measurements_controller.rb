module Api::V1
	class LightMeasurementsController < ApiController
		before_action :set_light_measurement, only: [:show, :update, :destroy]

	  # GET /light_measurements
	  def index
	    @light_measurements = LightMeasurement.all

	    render json: @light_measurements
	  end

	  # GET /light_measurements/1
	  def show
	    render json: @light_measurement
	  end

	  # POST /light_measurements
	  def create
	    @light_measurement = LightMeasurement.new(light_measurement_params)

	    if @light_measurement.save
	      render json: @light_measurement, status: :created
	    else
	      render json: @light_measurement.errors, status: :unprocessable_entity
	    end
	  end

	  # PATCH/PUT /light_measurements/1
	  def update
	    if @light_measurement.update(light_measurement_params)
	      render json: @light_measurement
	    else
	      render json: @light_measurement.errors, status: :unprocessable_entity
	    end
	  end

	  # DELETE /light_measurements/1
	  def destroy
	    @light_measurement.destroy
	  end

	  private
	    # Use callbacks to share common setup or constraints between actions.
	    def set_light_measurement
	      @light_measurement = LightMeasurement.find(params[:id])
	    end

	    # Only allow a trusted parameter "white list" through.
	    def light_measurement_params
	      params.require(:light_measurement).permit(:lux, :measure_time)
	    end
	end
end