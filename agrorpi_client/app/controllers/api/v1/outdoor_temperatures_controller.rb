module Api::V1
  class OutdoorTemperaturesController < ApiController
    before_action :set_outdoor_temperature, only: [:show, :update, :destroy]
    def index
      @outdoor_temperatures = OutdoorTemperature.all
    end
    
    def create
      @outdoor_temperature = OutdoorTemperature.new(outdoor_temperature_params)
      if @outdoor_temperature.save
        render json: @outdoor_temperature, status: :created
      else
        render json: @outdoor_temperature.errors, status: :unprocessable_entity
      end
    end

    private
    def set_outdoor_temperature
      @outdoor_temperature = OutdoorTemperature.find(params[:id])
    end

    def outdoor_temperature_params
      params.require(:outdoor_temperature).permit(:id, :celsius, :farenheit, :measured_at)
    end
  end
end
