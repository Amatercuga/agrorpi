module Api::V1
  class IndoorTemperaturesController < ApiController
    before_action :set_indoor_temperature, only: [:show, :update, :destroy]
    def index
      @indoor_temperatures = IndoorTemperature.all
    end
    
    def create
      @indoor_temperature = IndoorTemperature.new(indoor_temperature_params)
      if @indoor_temperature.save
        render json: @indoor_temperature, status: :created
      else
        render json: @indoor_temperature.errors, status: :unprocessable_entity
      end
    end

    private
    def set_indoor_temperature
      @indoor_temperature = IndoorTemperature.find(params[:id])
    end

    def indoor_temperature_params
      params.require(:indoor_temperature).permit(:id, :celsius, :farenheit, :measured_at)
    end
  end
end
