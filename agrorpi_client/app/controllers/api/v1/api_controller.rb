module Api::V1
  class ApiController < ApplicationController
    #API stuff here
    skip_before_filter  :verify_authenticity_token
  end
end