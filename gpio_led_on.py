from gpiozero import PWMLED
from time import sleep

led = PWMLED(21)

while True:
    led.value = 0  # off
    sleep(0.1)
    led.value = 0.1
    sleep(0.1)
    led.value = 0.2
    sleep(0.1)
    led.value = 0.3
    sleep(0.1)
    led.value = 0.4
    sleep(0.1)
    led.value = 0.5
    sleep(0.1)
    led.value = 0.6
    sleep(0.1)
    led.value = 0.7
    sleep(0.1)
    led.value = 0.8
    sleep(0.1)
    led.value = 0.9
    sleep(0.1)
    led.value = 1
    led.value = 0.9
    led.value = 0.8
    led.value = 0.7
    led.value = 0.6
    led.value = 0.5
    led.value = 0.4
    led.value = 0.3
    led.value = 0.2
    led.value = 0.1
    led.value = 0
    
