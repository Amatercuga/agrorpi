import time
from datetime import datetime
import sqlite3

conn = sqlite3.connect('baza')
c = conn.cursor()
#get cpu temp from db

for row in c.execute("SELECT * FROM cpu_temperatures"):
    #print(row[1])
    if row[1] < 50:
        print(str(row[1]) + " " + str(row[2]))
    

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()
