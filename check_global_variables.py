import os
import thread
from datetime import datetime
import time
import json
import agro_library
from gpiozero import PWMLED

pid = os.getpid()

with open('global_variables.json') as data_file:    
    data = json.load(data_file)

# Ucitaj varijable
__LUX_PIN = int(data["lux_senzor"])
__LED_1 = int(data["led_1"])
__LED_2 = int(data["led_2"])
__LED_1_INCREASEMENT = float(data["led_1_increasement"])


#methods
def check_lux(lux_sensor):
		sensor = lux_sensor
		#calculation
		#result = random.randint(1, 100)
		#koristim trenutnu vrijednost Ledice
		result = PWMLED(lux_sensor)
		return result


# Returning values
lux_result = 0


while True:
    #dohvati trenutnu vrijednost luxa
    lux_result = check_lux(__LUX_PIN)    
    #ako je manji od 0.8
    if lux_result < 0.8:
        print ("lux je manji")
        #povecavaj do zadane vrijednosti
        while lux_result == 0.8:
            try:
                lux_result += 0.1
                #thread.start_new_thread(agro_library.increase_lux, (__LED_1, __LED_1_INCREASEMENT))                
            except:
                print("error")

        #lux_result = agro_library.check_lux(__LUX_PIN)
        #print("trenutna vrijednost leda = " + str(lux_result))
            
        
		# dohvati trenutnu vrijednost luxa
		#lux_result = agro_library.check_lux(__LUX_PIN)
		#my_date = str(datetime.now())
		# spremi podatke pomocu novog threada
		#try:
		#		thread.start_new_thread(agro_library.save_lux, (lux_result, my_date) )
		#except:
		#	print("Error")
		
		#if lux_result > 85:
		#		#print("smanji svjetlo, lux = " + str(lux_result))
		#		agro_library.decrease_lux(__LED_1, 2)
		#elif lux_result <= 84:
		#		#print("pojacaj svjetlo, lux = " + str(lux_result))
		#		agro_library.increase_lux(__LED_1, 2)

		#agro_library.cpu_and_memory_usage(pid)
		# pricekaj 2 sekunde
		#time.sleep(2)
